var util = require('util');
var events = require("events");

var Clase = function() {
//	events.EventEmitter.call(this);
};

//Clase.prototype.__proto__ = events.EventEmitter.prototype;
//Clase.prototype = Object.create(events.EventEmitter.prototype);
util.inherits(Clase,events);

Clase.prototype.puntos = {
	marco: 5,
	gonzalo: 100
}

Clase.prototype.puntaje = 0;

Clase.prototype.calificar = function(puntos) {

	this.puntaje = puntos;

	if(puntos==this.puntos.marco) {
		this.emit('informa');
	}
	
	if(puntos==this.puntos.gonzalo) {
		this.emit('informa');
	}
}


module.exports = Clase;


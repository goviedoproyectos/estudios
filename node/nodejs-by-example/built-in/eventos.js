var ev = require('events');

var emitidor = new ev.EventEmitter();

var saludo = function() {
	console.log('Hola Maestro');
}

var despedida = function() {
	console.log('Gracias por todo, Gran Guru');
}

emitidor.on('hola', saludo);
emitidor.on('chao', despedida);

for(let i=0;i<10;i++) {
	if(i==2) emitidor.emit('hola');

	if(i==9) emitidor.emit('chao');
}

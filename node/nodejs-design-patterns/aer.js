function add(a,b,callback) {
	callback(a+b);
}


console.log('antes');
add(1,2,result=>console.log('Resultado es: '+result));
console.log('despues');

function addAsync(a,b,callback) {
	setTimeout(()=>callback(a+b),1000);
}


console.log('antes');
addAsync(1,2,result=>console.log('Resultado es: '+result));
console.log('despues');

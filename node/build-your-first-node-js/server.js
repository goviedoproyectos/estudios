var express = require('express');

var expressLayouts = require('express-ejs-layouts');
var bodyParser = require('body-parser');

var app = express();
var port = 3000;

//Seteamos el engine de la vista a EJS.
app.set('view engine', 'ejs');
app.use(expressLayouts);

//use body parser para leer las urls via POST GET PUT DELETE me imagino
app.use(bodyParser.urlencoded());

// route our app
var router = require('./app/routes');
app.use('/', router);

// Seteo de static files (css and images, etc) location
app.use(express.static(__dirname+'/public'));

// Start the server
app.listen(port, function() {
	console.log('La aplicacion ha comenzado como Express en 3000');
});

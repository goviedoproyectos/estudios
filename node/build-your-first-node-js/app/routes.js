// route para pagina principal
var express = require('express');
var path = require('path');
var router = express.Router();

module.exports = router;

router.get('/', function(req, res) {
	//res.sendFile(path.join(__dirname,'../index.html'));
	res.render('pages/index');
});

// route para pagina about
router.get('/about', function(req, res) {
	//res.sendFile(path.join(__dirname,'../about.html'));
	res.render('pages/about');
});

router.get('/contact', function(req, res) {
	//res.sendFile(path.join(__dirname,'../about.html'));
	res.render('pages/contact');
});

router.post('/contact', function(req, res) {
	var datos = {
		nombre: req.body.nombre,
		email: req.body.email,
		celular: req.body.celular
	};

	res.render('pages/recibido', {
		contacto: datos
	});
	//res.send('Excelente, lo catactaremos al e-mail (' + req.body.email + ') que nos ha proporcionado');
});


// route para pagina contacto

var http = require('http');

http.createServer(function(req, res) {
	res.writeHead(200,{'Content-Type':'text/plain'});
	res.end('Primeros pasos con Node js in Action');
}).listen(3000);

console.log('Servidor corriendo en el puerto 3000');

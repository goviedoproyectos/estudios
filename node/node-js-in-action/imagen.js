var http = require('http');
var rs = require('fs');

var servidor = http.createServer();

servidor.on('request', function(req,res) {
	res.writeHead(200,{'Content-Type':'image/jpeg'});
	rs.createReadStream('./desierto.jpg').pipe(res);
});

servidor.listen(4000);

console.log('Servidor en el puerto 4000 mostrando imagen mia.png');

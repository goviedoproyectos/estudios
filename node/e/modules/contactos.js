var fs = require('fs');
var url = require('url');
var _ = require('underscore');

function read_json_file() {
	var file = './data/contactos.json';
	return fs.readFileSync(file);
}

function listaCompleta() {
	return JSON.parse(read_json_file());
}

exports.lista = function() {
	return listaCompleta();
}

/**
 * Debe ser llamado de la forma ..../c/Joe
 */
exports.nombre = function(req,res) {
	var params = url.parse(req.url,true).query;

	var parametro = params.name;
	parametro = req.params.n;

	console.log('Parametro: '+parametro);

	var jparse = listaCompleta();

	var resultado = jparse.result;

	var objeto={};

	for(var i=0;i<resultado.length;i++) {
		var contacto = resultado[i];
		if(contacto.firstname==parametro) {
			objeto = {
				nombre:contacto.firstname,
				apellido:contacto.lastname,
				telefono:contacto.primarycontactnumber
			};
		}
	}

	if(!_.isEmpty(objeto)) {
		res.render('c',objeto);
	} else {
		res.render('index',{title:'No encontrado'});
	}
}

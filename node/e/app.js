
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var contactos = require('./modules/contactos'); 
var http = require('http');
var path = require('path');

var logger = require('morgan');
//var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var url = require('url');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.session({ secret: 'your secret here' }));
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);
app.get('/c/:n', contactos.nombre);


//Servicios

app.get('/contactos',
		function(req, resp) {
			var get_params = url.parse(req.url,true).query;

			if(Object.keys(get_params).length===0) {

				resp.setHeader('content-type','application/json');
				// null, 3 lo embellece
				resp.end(JSON.stringify(contactos.lista(),null,3));

			}
		}
);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

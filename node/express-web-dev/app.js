var http = require('http');
var express = require('express');
var fs = require('fs');
var app = new express();

//Log a fichero

app.use(express.logger({
	format:'dev',
	stream:fs.createWriteStream('app.log',{
		'flags':'w'
	})
}));

var rutas = require('./routes')(app);

//Faltara explicitamente declarar routes?

app.set('view engine','jade');
app.set('views','./vistas');

app.use(express.static('./public'));

// En teoria debiera mostrar el tiempo de respuesta
app.use(express.responseTime());

//Errores mejor presentados
app.use(express.errorHandler());

http.createServer(app).listen(3000,function() {
	console.log('Servidor Express a la escucha en 3000');
});


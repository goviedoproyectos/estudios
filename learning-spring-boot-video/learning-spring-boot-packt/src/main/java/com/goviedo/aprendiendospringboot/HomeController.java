package com.goviedo.aprendiendospringboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by goviedo on 2/24/17.
 */
@Controller
public class HomeController {

    private static final String BASE_PATH = "/images";
    private static final String BASE_PATH_CURL = "/curl";
    private static final String FILENAME = "{filename:.+}";

    private final  ImageService imageService;

    @Autowired
    public HomeController(ImageService imageService) {
        this.imageService = imageService;
    }

    @RequestMapping("/")
    public String index(Model model, Pageable pageable) {
        final Page<Image> page = imageService.findPage(pageable);
        model.addAttribute("page",page);

        if(page.hasNext()) {
            model.addAttribute("next",pageable.next());
        }

        if(page.hasPrevious()) {
            model.addAttribute("prev", pageable.previousOrFirst());
        }
        return "index";
    }


    @RequestMapping(method = RequestMethod.GET, value = BASE_PATH + "/" + FILENAME + "/raw")
    @ResponseBody
    public ResponseEntity<?> oneRawImage(@PathVariable String filename) {
        try {
            Resource image = imageService.findOneImage(filename);
            return ResponseEntity.ok().contentLength(image.contentLength())
                    .contentType(MediaType.IMAGE_PNG)
                    .body(new InputStreamResource(image.getInputStream()));
        } catch (IOException ex) {
            return ResponseEntity.badRequest()
                    .body("No encontre el "+filename + " => "+ex.getMessage());

        }
    }

    /**
     * Subir archivos sin FORM, por ejemplo con CURL
     * @param file
     * @param servletRequest
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = BASE_PATH_CURL)
    @ResponseBody
    public ResponseEntity<?> createFile(@RequestParam("file")MultipartFile file, HttpServletRequest servletRequest) {
        try {
            imageService.createImage(file);
            final URI locationUri = new URI(servletRequest.getRequestURL().toString()+"/")
                    .resolve(file.getOriginalFilename()+"/raw");
            return ResponseEntity.created(locationUri)
                    .body("Se ha subido satisfactoriamente " + file.getOriginalFilename() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Fallo al subir el archivo" + file.getOriginalFilename() + "=>" + e.getMessage());

        } catch (URISyntaxException ex) {
            return  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Problemas al subir el archivo  "+ file.getOriginalFilename() + "=> "+ex.getMessage());
        } catch(Exception e1) {
            return  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Problemas al subir el archivo  "+ file.getOriginalFilename() + "=> "+e1.getMessage());
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = BASE_PATH)
    public String createFile(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
        try {
            imageService.createImage(file);
            redirectAttributes.addFlashAttribute("flash.message", "Bien, tenemos la imagen... La puedes ver: " + file.getOriginalFilename());
        } catch (IOException e) {
            e.printStackTrace();
            redirectAttributes.addFlashAttribute("flash.message", "Wuooooh, que pasoo!!!!... Vamos intentalo de nuevo");
        }
        return "redirect:/";
    }

    /**
     * Eliminacion mediante CURL
     * @param filename
     * @return
     */
    @RequestMapping(method = RequestMethod.DELETE, value = BASE_PATH_CURL + "/" + FILENAME)
    @ResponseBody
    public ResponseEntity deleteFile(@PathVariable String filename) {
        try {
            imageService.deleteImage(filename);
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body("Se ha borrado satisfactoriamente " + filename);
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("No ha sido posible borrar el archivo "+filename+ " => "+ e.getMessage());
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = BASE_PATH + "/" + FILENAME)
    public String deleteFile(@PathVariable String filename, RedirectAttributes redirectAttributes) {
        try {
            imageService.deleteImage(filename);
            redirectAttributes.addFlashAttribute("flash.message", "Dios!, you're evil!. Erased!, Bye Bye "+filename);
        } catch (IOException e) {
            e.printStackTrace();
            redirectAttributes.addFlashAttribute("flash.message", "Wuoooh... Damn esa imagen esta de suerte!... No se borró");
        } catch (Exception ex) {
            ex.printStackTrace();
            redirectAttributes.addFlashAttribute("flash.message", "Wuoooh... Damn esa imagen esta de suerte!... No se borró");
        }
        return "redirect:/";
    }
}

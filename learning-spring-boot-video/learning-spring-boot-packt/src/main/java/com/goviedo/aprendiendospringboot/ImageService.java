package com.goviedo.aprendiendospringboot;

import org.apache.tomcat.util.http.fileupload.FileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by goviedo on 2/24/17.
 */
@Service
public class ImageService {

    private static String ROOT_IMAGE = "upload-image-dir";

    private final ImageRepository imageRepository;
    private final ResourceLoader resourceLoader;

    @Autowired
    public ImageService(ImageRepository imageRepository, ResourceLoader resourceLoader) {
        this.imageRepository = imageRepository;
        this.resourceLoader = resourceLoader;
    }

    public Resource findOneImage(String fileName) {
        return resourceLoader.getResource("file:" + ROOT_IMAGE + '/' + fileName);
    }

    public void createImage(MultipartFile file) throws IOException {
        if(!file.isEmpty()) {
            Files.copy(file.getInputStream(), Paths.get(ROOT_IMAGE, file.getOriginalFilename()));
            imageRepository.save(new Image(file.getOriginalFilename()));
        }
    }

    public void deleteImage(String fileName) throws IOException {
        final Image image = imageRepository.findByName(fileName);
        imageRepository.delete(image);
        Files.deleteIfExists(Paths.get(ROOT_IMAGE,fileName));
    }

    public Page<Image> findPage(Pageable pageable) {
        return imageRepository.findAll(pageable);
    }

    @Bean
    CommandLineRunner setUp(ImageRepository repository) throws IOException {

        return (args) -> {

            FileSystemUtils.deleteRecursively(new File(ROOT_IMAGE));

            Files.createDirectory(Paths.get(ROOT_IMAGE));

            /*
            FileCopyUtils.copy("Prueba 1", new FileWriter(ROOT_IMAGE + "/prueba"));
            repository.save(new Image("prueba"));

            FileCopyUtils.copy("Prueba 2", new FileWriter(ROOT_IMAGE + "/prueba2"));
            repository.save(new Image("prueba2"));
            */
        };
    }
}

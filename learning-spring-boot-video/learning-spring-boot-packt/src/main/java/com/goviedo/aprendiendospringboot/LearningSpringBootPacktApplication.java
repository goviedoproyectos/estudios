package com.goviedo.aprendiendospringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearningSpringBootPacktApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearningSpringBootPacktApplication.class, args);
	}
}

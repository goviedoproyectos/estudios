package com.goviedo.aprendiendospringboot;

import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by goviedo on 2/24/17.
 */
public interface ImageRepository extends PagingAndSortingRepository<Image, Long> {
    public Image findByName(String name);
}

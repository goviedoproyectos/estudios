(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

exports.generar = generar;

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Caracteres = exports.Caracteres = function () {
  function Caracteres(texto) {
    _classCallCheck(this, Caracteres);

    this.name = texto;
  }

  _createClass(Caracteres, [{
    key: "showUp",
    value: function showUp() {
      return this.name;
    }
  }]);

  return Caracteres;
}();

var Bear = exports.Bear = function (_Caracteres) {
  _inherits(Bear, _Caracteres);

  function Bear() {
    _classCallCheck(this, Bear);

    return _possibleConstructorReturn(this, (Bear.__proto__ || Object.getPrototypeOf(Bear)).call(this, "Aggggrrrr. I'm a great Bear... will tear you apart Fucker!"));
  }

  return Bear;
}(Caracteres);

var Angel = exports.Angel = function (_Caracteres2) {
  _inherits(Angel, _Caracteres2);

  function Angel() {
    _classCallCheck(this, Angel);

    return _possibleConstructorReturn(this, (Angel.__proto__ || Object.getPrototypeOf(Angel)).call(this, "I'm Gonzalo, the greatest Angel of all... I will give you life!"));
  }

  return Angel;
}(Caracteres);

var Burro = exports.Burro = function (_Caracteres3) {
  _inherits(Burro, _Caracteres3);

  function Burro() {
    _classCallCheck(this, Burro);

    return _possibleConstructorReturn(this, (Burro.__proto__ || Object.getPrototypeOf(Burro)).call(this, "Hi....They call me, Burro, I will reap you... prepare your ass!"));
  }

  return Burro;
}(Caracteres);

var Ghost = exports.Ghost = function (_Caracteres4) {
  _inherits(Ghost, _Caracteres4);

  function Ghost() {
    _classCallCheck(this, Ghost);

    var _this4 = _possibleConstructorReturn(this, (Ghost.__proto__ || Object.getPrototypeOf(Ghost)).call(this));

    _this4.name = "I'm a fucker Ghost, i will suck your soul right now...!";
    return _this4;
  }

  return Ghost;
}(Caracteres);

function generar() {
  var numero = Math.floor(Math.random() * (4 - 1)) + 1;

  switch (numero) {
    case 1:
      return new Bear();break;
    case 2:
      return new Burro();break;
    case 3:
      return new Ghost();break;
    case 4:
      return new Angel();break;
    default:
      break;
  }
}

},{}],2:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Enviroment = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); //import {Bear} from "./caracteres.js";


var _caracteres = require("./caracteres.js");

var caracteres = _interopRequireWildcard(_caracteres);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Enviroment = exports.Enviroment = function () {
  function Enviroment(name) {
    _classCallCheck(this, Enviroment);

    this.name = name;
    this.encounter = caracteres.generar();
  }

  _createClass(Enviroment, [{
    key: "stumbleUpon",
    value: function stumbleUpon() {
      this.encounter = caracteres.generar();
      return this.name + ' ' + this.encounter.showUp();
    }
  }]);

  return Enviroment;
}();

},{"./caracteres.js":1}],3:[function(require,module,exports){
"use strict";

var _enviroment = require("./enviroment.js");

var _utils = require("./utils.js");

var TITULO = "Escribe las weas que queari CTM";
var BR = '<br/>';

function main() {
  var enterEl = document.querySelector("#enter");
  console.log('agrego evento');
  enterEl.addEventListener("click", onClickEnter, false);
  addToOutput();
}

var env = new _enviroment.Enviroment('Bosque - ');

function commands(newLine) {
  switch (newLine) {
    case "help":
      _utils.ayuda.opciones();break;
    case "up":
      addToOutput(env.stumbleUpon());break;
    case "down":
      addToOutput(env.stumbleUpon());break;
    case "left":
      addToOutput(env.stumbleUpon());break;
    case "right":
      addToOutput(env.stumbleUpon());break;
    default:
      output.innerHTML = output.innerHTML + BR + newLine;break;
  }
}

function addToOutput() {
  var newLine = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : TITULO;

  var output = document.querySelector("#output");
  commands(newLine);
}

function onClickEnter() {
  var commands = document.querySelector("#commands");

  addToOutput(commands.value);
  console.log('Evento enter:' + commands.value);
}

main();

},{"./enviroment.js":2,"./utils.js":4}],4:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var ayuda = exports.ayuda = {
  _p: 'Puede hacer ',
  _h: ["up", "down", "left", "right"],
  opciones: function opciones() {
    var _this = this;

    this._h.forEach(function (f) {
      return addToOutput(_this._p + ' ' + f);
    });
  }
};

},{}]},{},[3]);

"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var ayuda = exports.ayuda = {
  _p: 'Puede hacer ',
  _h: ["up", "down", "left", "right"],
  opciones: function opciones() {
    var _this = this;

    this._h.forEach(function (f) {
      return addToOutput(_this._p + ' ' + f);
    });
  }
};
//# sourceMappingURL=utils.js.map
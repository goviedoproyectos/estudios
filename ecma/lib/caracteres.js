"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

exports.generar = generar;

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Caracteres = exports.Caracteres = function () {
  function Caracteres(texto) {
    _classCallCheck(this, Caracteres);

    this.name = texto;
  }

  _createClass(Caracteres, [{
    key: "showUp",
    value: function showUp() {
      return this.name;
    }
  }]);

  return Caracteres;
}();

var Bear = exports.Bear = function (_Caracteres) {
  _inherits(Bear, _Caracteres);

  function Bear() {
    _classCallCheck(this, Bear);

    return _possibleConstructorReturn(this, (Bear.__proto__ || Object.getPrototypeOf(Bear)).call(this, "Aggggrrrr. I'm a great Bear... will tear you apart Fucker!"));
  }

  return Bear;
}(Caracteres);

var Angel = exports.Angel = function (_Caracteres2) {
  _inherits(Angel, _Caracteres2);

  function Angel() {
    _classCallCheck(this, Angel);

    return _possibleConstructorReturn(this, (Angel.__proto__ || Object.getPrototypeOf(Angel)).call(this, "I'm Gonzalo, the greatest Angel of all... I will give you life!"));
  }

  return Angel;
}(Caracteres);

var Burro = exports.Burro = function (_Caracteres3) {
  _inherits(Burro, _Caracteres3);

  function Burro() {
    _classCallCheck(this, Burro);

    return _possibleConstructorReturn(this, (Burro.__proto__ || Object.getPrototypeOf(Burro)).call(this, "Hi....They call me, Burro, I will reap you... prepare your ass!"));
  }

  return Burro;
}(Caracteres);

var Ghost = exports.Ghost = function (_Caracteres4) {
  _inherits(Ghost, _Caracteres4);

  function Ghost() {
    _classCallCheck(this, Ghost);

    var _this4 = _possibleConstructorReturn(this, (Ghost.__proto__ || Object.getPrototypeOf(Ghost)).call(this));

    _this4.name = "I'm a fucker Ghost, i will suck your soul right now...!";
    return _this4;
  }

  return Ghost;
}(Caracteres);

function generar() {
  var numero = Math.floor(Math.random() * (4 - 1)) + 1;

  switch (numero) {
    case 1:
      return new Bear();break;
    case 2:
      return new Burro();break;
    case 3:
      return new Ghost();break;
    case 4:
      return new Angel();break;
    default:
      break;
  }
}
//# sourceMappingURL=caracteres.js.map
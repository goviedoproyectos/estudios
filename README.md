Repositorio dedicado exclusivamente para pruebas de conceptos o estudios de diferentes lenguajes o tecnologias.

Me sirve tambien como repositorio de codigos utiles que se pueden utilizar en posterioridad.

Carpeta node/

e/ -> Estudio de Node Express

build-your-first-node-js -> Una aplicacion de 0 con un formulario con nodejs express, routing, ejs layouts, body parser.

nodejs-by-example:
	Estudio de nodejs by example de Node Fundamentals:  https://www.packtpub.com/mapt/book/application_development/9781784395711/1/ch01lvl1sec09/Installing+Node.js

Carpeta /

responsive-web-design/ -> Estudio de Diseño Responsive de Páginas Web (Adaptaciones a Dispositivos) usando solo CSS

ecma/ -> Nueva especificacion Javascript ECMCA 6

vue-cli/ -> Estudio de VUE JS 2.0 de Udemy del instructor ...

mud/ -> Aprendiendo ECMA6 con un juego Multiusuario

stormpath -> Estudio y codigos relacionados con el servicio de Autenticacion con seguridad y perfiles de Stormpath.com

learning-spring-boot-video/ -> De PACKT. Construccion de un sitio simple de imagenes a subir. Ademas da excelentes caracteristicas de Spring Boot.

import Vue from 'vue'
import App from './App.vue'
import Cabecera from './components/Cabecera.vue'
import Pie from './components/Pie.vue'
import Cuerpo from './components/Cuerpo.vue'

//Vue.component('saludo',Saludo);

Vue.component('Cabecera',Cabecera);
Vue.component('Pie',Pie);
Vue.component('Cuerpo',Cuerpo)

new Vue({
  el: '#app',
  render: h => h(App)
})
